Matej

# HTTP
### (HyperText  Transfer Protocol)

On je glavna i najčešća metoda prijenosa informacija na Webu. Osnovna namjena ovog protokola je omogućavanje objavljivanja i prezentacije HTML dokumenata, tj. web stranica.
HTTP je samo jedan od protokola aplikativne razine koji postoje na Internetu. Drugi značajniji internetski protokoli na aplikacijskoj razini su:
1. [FTP] (https://hr.wikipedia.org/wiki/FTP)
2. [HTTP] (https://hr.wikipedia.org/wiki/HTTP)
3. [HTTPS] (https://hr.wikipedia.org/wiki/HTTPS)
4. [IMAP] (https://hr.wikipedia.org/wiki/IMAP)
5. [IRC] (https://hr.wikipedia.org/wiki/IRC)
6. [POP3] (https://hr.wikipedia.org/wiki/POP3)

Slika lisice :
![alt text](http://www.turistplus.hr/upload/katalog/527122502(2).jpg "Super kul slika lisice")

| MOJA     | PRVA       | TABLICA  |
| ------------- |:-------------:| ---------------:|
| lisice       | su kul      | 1337           |
| nesto      | bezveze|   39393       |


```python
print "Hello World!"
print "Hello Again"
print "I like typing this."
print "This is fun."
print 'Yay! Printing.'
print "I'd much rather you 'not'."
print 'I "said" do not touch this.'
```